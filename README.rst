===============================================================
Zabbix の監視に際して、AWS VGW のデータを収集する補助スクリプト
===============================================================

Zabbix で AWS VGW で構築される VPN コネクションを監視を補助するふたつのスクリプトです。

スクリプトのひとつは、AWS の VPN コネクションの情報を取得して、Zabbix のトラッパー タイプのディスカバリに通知します。
ディスカバリは受け取った情報を使ってトラッパー タイプのアイテム等を生成します。

もうひとつは、AWS の VPN コネクションの状態とそこを通過する通信量を取得して、Zabbix のトラッパー タイプのアイテムに登録します。

------------------------
スクリプトのインストール
------------------------

pip を使って以下のように実行すると、Python のスクリプト ディレクトリに、ふたつのスクリプト `aws.vpn.discovery` と `aws.vpn.sender` がインストールされます。

.. code:: shell

    pip install zbx-awsvpn

ただし現時点では PYPI に未登録なため、以下のように git リポジトリを直接指定してインストールします。

.. code:: shell

    pip install git+https://ryuu@bitbucket.org/ryuu/zbx-awsvpn.git

----------------
スクリプトの実行
----------------

cron などを用いた定期的な実行を想定しています。
以下の例では、`aws.vpn.discovery` を毎時 5分に、`aws.vpn.sender` を毎時 2分、22分、42分に実行します。

crontab の例::

    AWSVPN=/etc/zabbix/awsvpc/bin
    6 *         * * *   root    $AWSVPN/aws.vpn.discovery
    2-59/20 *   * * *   root    $AWSVPN/aws.vpn.sender

ディスカバリ登録スクリプト
--------------------------

Usage: aws.vpn.discovery [OPTIONS]

Options:
  -r, --uri URI          Zabbix base URI.  [default: http://localhost/zabbix/]
  -l, --ssl              Force SSL on Zabbix API URI.  [default: False]
  -u, --user USER ID     Login user name.  [default: api]
  -p, --passwd PASSWORD  Login user password  [default: zabbix]
  -v, --verbose          Logging level, assigned -v or -vv.
  --help                 Show this message and exit.

VPN 状態／通信量登録スクリプト
------------------------------

Usage: aws.vpn.sender [OPTIONS]

Options:
  -r, --uri uri          Zabbix API URI.  [default: http://localhost/zabbix/]
  -l, --ssl              Force SSL on Zabbix API URI.  [default: False]
  -u, --user USER ID     Login user name.  [default: api]
  -p, --passwd PASSWORD  Login user password  [default: zabbix]
  -v, --verbose          Logging level, assigned -v or -vv.
  --help                 Show this message and exit.

----------------
スクリプトの動作
----------------

ディスカバリ登録スクリプト
--------------------------

このスクリプトでは最初に Zabbix API を用いて以下のキーで定義されているトラッパー タイプのディスカバリを持つホストとそのホストで定義されている以下のマクロの値を取得します。[#parms]_

- ディスカバリのキー
    - `aws.vpn.discovery`
- マクロ
    - `{$AWS_PROFILE}`
        AWS API を実行するときのプロファイル名を指定します。
    - `{$AWS_VPG_VGW}`
        監視対象の AWS の VPC の Name タグの値、VPC の ID、VGW の Name タグの値、VGW の ID のどれかひとつを、どれを指定したかを明示する接頭辞をつけて指定します。

        - VPC の Name タグの値を記述するときは ID に `VPC_NM:` の 7 文字の接頭辞を付けます。
        - VPC の ID を記述するときは ID に `VPC_ID:` の 7 文字の接頭辞を付けます。
        - VGW の Name タグの値を記述するときは ID に `VGW_NM:` の 7 文字の接頭辞を付けます。
        - VGW の ID を記述するときは ID に `VGW_ID:` の 7 文字の接頭辞を付けます。

取得した各ホストのプロファイル名や VPC の Name タグの値などを元に、AWS API を用いて収集した情報を以下の構造にして Zabbix Sender プロトコルでそれぞれのホストへのデータとして通知します。[#aws]_ 

- `{#VPN_CON_ID}`
    VPN コネクションの ID。
- `{#VPN_CON_NAME}`
    VPN コネクションの Name タグの値。Name タグが定義されていないときは NONAME。
- `{#VPN_CON_ADDR}`
    VPN コネクションのゲートウェイ アドレス。
- `{#VPN_ID}`
    VPN コネクションの ID。
- `{#VPN_NAME}`
    VPN コネクションの Name タグの値。Name タグが定義されていないときは NONAME。

なお、`{#VPN_ID}` と `{#VPN_NAME}` の組み合わせ、`{#VPN_CON_ID}` と `{#VPN_CON_NAME}` と `{#VPN_CON_ADDR}` の組み合わせで情報が構成され、これ以外の組み合わせになることはありません。

Zabbix のディスカバリはこれらの値を使って以下のトラッパー アイテムを作成すると想定します。[#key]_\ [#trapper]_

- VPN の ID が `{#VPN_CON_ID}` で アドレスが `{#VPN_CON_ADDR}` のインターフェースの状態
    - アイテム名
        VPN connection status at AWS VPN `{#VPN_CON_NAME}` [`{#VPN_CON_ADDR}`]
    - キー
        `aws.vpnOperStatus[{#VPN_CON_ID},{#VPN_CON_ADDR}]`
    - データ型
        整数
    - 値のマッピング
        Service state
- ID が `{#VPN_ID}` の VPN を内に向かって通過する通信量
    - アイテム名
        Incoming traffic on AWS VPN `{#VPN_NAME}`
    - キー
        `aws.vpnInOctets[{#VPN_ID}]`
    - データ型
        整数
    - 単位
        Bytes/sec
- ID が `{#VPN_ID}` の VPN を内に向かって通過した累積通信量
    - アイテム名
        Incoming traffic on AWS VPN `{#VPN_NAME}`
    - キー
        `aws.vpnInVolume[{#VPN_ID}]`
    - データ型
        整数
    - 単位
        Bytes
- ID が `{#VPN_ID}` の VPN を外に向かって通過する通信量
    - アイテム名
        Outgoming traffic on AWS VPN `{#VPN_NAME}`
    - キー
        `aws.vpnOutOctets[{#VPN_ID}]`
    - データ型
        整数
    - 単位
        Bytes/sec
- ID が `{#VPN_ID}` の VPN を外に向かって通過した累積通信量
    - アイテム名
        Outgoming traffic on AWS VPN `{#VPN_NAME}`
    - キー
        `aws.vpnOutVolume[{#VPN_ID}]`
    - データ型
        整数
    - 単位
        Bytes

VPN 状態／通信量登録スクリプト
------------------------------

このスクリプトでは最初に Zabbix API を用いて以下のキーで定義されているトラッパー タイプのアイテムとこれらを持つホスト、そのホストで定義されているマクロの値を取得します。[#parms]_\ [#key]_

- アイテムのキー
    - `aws.vpnOperStatus[vpn_id, vpn_address}]`
        - vpn_id
            VPN コネクションの ID
        - vpn_address
            VPN コネクションに割り当てられている仮想ゲートウェイの外部アドレス
    - `aws.vpnInOctets[vpn_id]`
        - vpn_id
            VPN コネクションの ID
    - `aws.vpnInVolume[vpn_id]`
        - vpn_id
            VPN コネクションの ID
    - `aws.vpnOutOctets[vpn_id]`
        - vpn_id
            VPN コネクションの ID
    - `aws.vpnOutVolume[vpn_id]`
        - vpn_id
            VPN コネクションの ID
- マクロ
    - `{$AWS_PROFILE}`
        AWS API を実行するときのプロファイル名を指定します。

取得した各ホストのプロファイル名やアイテム キーに含まれる引数を元に AWS API を用いて情報を収集して、
最初に取得したトラッパー- アイテムに Zabbix Sender プロトコルで登録します。[#aws]_\ [#trapper]_

--------------------------
スクリプト使用時の注意事項
--------------------------

..  [#parms] 本スクリプトが使う Zabbix API のユーザーに、対象のホストに対する読み取り権限を予め付与してください。

..  [#aws] 本スクリプトを実行するユーザーのホーム ディレクトリに `.aws` ディレクトリを作成して、
    そこに AWS API の Key ID と Access Key およびこれに関するプロファイルの設定を保存しておいてください。
    このためスクリプトの実行時にはスクリプトが実行ユーザーのホーム ディレクトリを正しく参照できなくてはならず、
    `sudo` では `-H` オプションの使用が推奨されます。

    `.aws` ディレクトリ設定例::

        $ cat ~/.aws/credentials
        [default]
        aws_access_key_id = foo
        aws_secret_access_key = bar
        
        $ cat ~/.aws/config
        [default]
        region = ap-northeast-1
        output = text
        [profile PF1]
        role_arn = arn:aws:iam::AWS_ID1:role/USER_ROLE
        region = ap-northeast-1
        source_profile = default
        [profile PF2]
        role_arn = arn:aws:iam::AWS_ID2:role/USER_ROLE
        region = ap-northeast-1
        source_profile = default

    Zabbix のディスカバリで作成するトラッパ アイテムには、本スクリプトを実行するホストからのデータ登録を許可しておいてください。

..  [#key] ディスカバリ登録スクリプトで返される値を使った Zabbix のディスカバリによる作成が想定されているアイテムと、
    VPN 状態／通信量登録スクリプトの取得対象のアイテムのキーは同一です。

..  [#trapper] ディスカバリ登録スクリプトで収集するディスカバリで作成し、VPN 状態／通信量登録スクリプトでデータを収集・登録するトラッパー アイテムは、VPN 状態／通信量登録スクリプトを実行するホストからのデータ登録を許可してください。
