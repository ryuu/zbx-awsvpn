#!/opt/boto3/bin/python
#-*- coding: utf-8 -*-

import click
import boto3
import contextlib
from functools import wraps
import json
from logging import basicConfig as loggingBasicConfig, getLogger, StreamHandler, WARNING, INFO, DEBUG
from pathlib import Path
import pprint
from pyzabbix.sender import ZabbixMetric, ZabbixSender
import sqlite3
import textwrap
from urllib.error import URLError
from urllib.parse import urlsplit, urlunsplit
from .zbxapi import ZabbixApi

MACRO_PROFILE = '{$AWS_PROFILE}'
MACRO_VPCVGW = '{$AWS_VPC_VGW}'
VPCVGW_PREFIX = ('vpc_nm', 'vpc_id', 'vgw_nm', 'vgw_id')
DISCOVERY_KEY = 'aws.vpn.discovery'

def add_logger(func):
    BASENAME = None if __name__ is None else Path(__file__).stem if __name__== '__main__' else __name__
    logger = getLogger(BASENAME)
    if not logger.hasHandlers():
        logger.addHandler(StreamHandler())
    if func.__name__ != 'main':
        logger = logger.getChild(func.__name__)
    @wraps(func)
    def wrapper(*args, **kwds):
        return func(logger, *args, **kwds)
    return wrapper

@click.command()
@click.option('-r', '--uri', metavar='URI', default='http://localhost/zabbix/',
              help='Zabbix base URI.', show_default=True)
@click.option('-l', '--ssl', 'force_ssl', is_flag=True, default=False,
              help='Force SSL on Zabbix API URI.', show_default=True)
@click.option('-u', '--user', 'userid', metavar='USER ID', default='api',
              help='Login user name.', show_default=True)
@click.option('-p', '--passwd', metavar='PASSWORD', default='zabbix',
              help='Login user password', show_default=True)
@click.option('-t', '--send-to', 'send_to', default='localhost:10051',
              help='Zabbix Server address and port.', show_default=True)
@click.option('-v', '--verbose', count=True, default=0,
              help='Logging level, assigned -v or -vv.')
@add_logger
def main(logger, uri, force_ssl, userid, passwd, send_to, verbose):
    logger.setLevel({0: WARNING, 1: INFO, 2: DEBUG}.get(verbose, DEBUG))
    if force_ssl:
        uri_parts = urlsplit(uri, scheme='http')
        if uri_parts.scheme == 'http':
            uri = urlunsplit(['https'] + list(uri_parts[1:]))
    with create_database() as db:
        collect_discovery_infomation_from_zbx(db, uri, userid, passwd)
        collect_aws_vpn_infomation(db)
        packet = build_sender_metrics(db)
    if packet:
        sender_params = dict(
            zabbix_server='localhost',
            zabbix_port=10051,
        )
        send_to = send_to.rsplit(':', 1)
        if send_to:
            if send_to[0]:
                sender_params['zabbix_server'] = send_to[0]
            if len(send_to) > 1 and send_to[1].isdecimal():
                sender_params['zabbix_port'] = int(send_to[1])
        logger.info(f'send packet to "{sender_params["zabbix_server"]}:{sender_params["zabbix_port"]}"')
        result = ZabbixSender(**sender_params).send(packet)
        if result.failed:
            logger.error(f'{result}')
        else:
            logger.debug(f'{result}')

@add_logger
def create_database(logger):
    db = sqlite3.connect(':memory:', isolation_level=None)
    db.row_factory = sqlite3.Row
    sql_create_table = textwrap.dedent("""
        CREATE TABLE host_vpcvgw_prof (
            hostid INTEGER PRIMARY KEY ON CONFLICT IGNORE,
            host TEXT,
            vpcvgw TEXT,
            profile TEXT,
            UNIQUE (host) ON CONFLICT IGNORE
        );
        CREATE INDEX idx_host ON host_vpcvgw_prof (host);
        CREATE INDEX idx_vpcvgw ON host_vpcvgw_prof (vpcvgw);
        CREATE INDEX idx_prof ON host_vpcvgw_prof (profile);

        CREATE TABLE vpcvgw_vpn (
            vpcvgw TEXT,
            vpn_id TEXT,
            vpn_nm TEXT,
            PRIMARY KEY (vpcvgw, vpn_id) ON CONFLICT IGNORE
            UNIQUE (vpn_id, vpn_nm) ON CONFLICT IGNORE
        );

        CREATE TABLE vpn_addr (
            vpn_id TEXT,
            addr TEXT,
            PRIMARY KEY (vpn_id, addr) ON CONFLICT IGNORE
        );
    """)
    for prefix in VPCVGW_PREFIX:
        sql_create_table += textwrap.dedent(f"""
            CREATE TABLE vpcvgw_{prefix} (
                vpcvgw TEXT,
                {prefix} TEXT,
                PRIMARY KEY (vpcvgw, {prefix}) ON CONFLICT IGNORE
            );
            CREATE INDEX idx_{prefix} ON vpcvgw_{prefix} ({prefix});
        """);
    with contextlib.closing(db.cursor()) as cur:
        cur.executescript(sql_create_table)
        if logger.getEffectiveLevel() >= DEBUG:
            cur.execute('select tbl_name from sqlite_master WHERE type="table";')
            for row in cur:
                logger.debug(f'sql table "{row["tbl_name"]}" created')
    return db

@add_logger
def collect_discovery_infomation_from_zbx(logger, db, uri, userid, passwd):
    """
    引数の uri, userid, passwd を使って Zabbix API にアクセスして、
    aws.vpn.discovery がキーであるディスカバリを持つホストをすべて取得し
    て、さらにそのホストのマクロから AWS のプロファイル名と VPC_VGW を取
    得したうえで、db に登録します。
    """
    try:
#        api = ZabbixAPI(uri, user=userid, password=passwd)
        api = ZabbixApi(uri, user=userid, password=passwd, verify=False)
        logger.info(f'access to Zabbix API {uri} with "{userid}"')
    except URLError:
        logger.error(f'access to Zabbix API {uri} with "{userid}"')
        raise
    with contextlib.closing(db.cursor()) as cur:
        # 特定のキーで指定されるディスカバリを持つホストの一覧を取得す
        # る。
        method = 'discoveryrule.get'
        params = {
            'monitored': True,
            'selectHosts': ['hostid', 'host'],
            'filter': {
                'key_': [DISCOVERY_KEY,],
                'type': 2,  #-- 2 is Zabbix trapper
            },
            'output': ['itemid', 'hosts'],
        }
        for item in api.do_request(method, params):
            for host_info in item.get('hosts'):
                hostid, host = host_info.get('hostid'), host_info.get('host')
                if hostid and host:
                    cur.execute(
                        'INSERT INTO host_vpcvgw_prof (hostid, host)'
                        '   VALUES (?, ?)', (hostid, host)
                    )
                    logger.debug(f'add HOST <{hostid} {host}> to host_vpcvgw_prof')
        # 取得したホストで定義されている特定のマクロの一覧を取得する。
        cur.execute('select hostid from host_vpcvgw_prof;')
        method = 'usermacro.get'
        params = {
            'hostids': [row['hostid'] for row in cur.fetchall()],
            'filter': {
                'macro': [MACRO_PROFILE, MACRO_VPCVGW],
            },
            'output': ['hostmacroid', 'hostid', 'macro', 'value'],
        }
        for usermacro in api.do_request(method, params):
            hostid , macro, value = usermacro.get('hostid'), usermacro.get('macro'), usermacro.get('value')
            if hostid and macro and value:
                if macro == MACRO_PROFILE:
                    cur.execute('UPDATE host_vpcvgw_prof SET profile = ? WHERE hostid = ?;', (value, hostid))
                    logger.debug(f'update HOST PROF <{hostid} {value}> to host_vpcvgw_prof')
                elif macro == MACRO_VPCVGW:
                    try:
                        kind = value[:6].lower()
                        id_nm = value[7:]
                        if value[6] == ':' and kind in VPCVGW_PREFIX:
                            cur.execute('UPDATE host_vpcvgw_prof SET vpcvgw = ? WHERE hostid = ?;', (value, hostid))
                            logger.debug(f'update HOST VPCVGW <{hostid} {value}> to host_vpcvgw_prof')
                            cur.execute(f'INSERT INTO vpcvgw_{kind} (vpcvgw, {kind}) VALUES (?, ?)', (value, id_nm))
                            logger.debug(f'add VPCVGW {kind.upper()} <{value} {id_nm}> to vpcvgw_{kind}')
                    except IndexError:
                        pass

@add_logger
def collect_aws_vpn_infomation(logger, db):
    """
    AWS API を使って vgw_id、vgw_nm、vpc_id、vpn_nm の値から、VPN ID とそ
    の VPN のアドレスの一覧を取得します。
    """
    with contextlib.closing(db.cursor()) as cur:
        for profile_row in cur.execute('SELECT profile FROM host_vpcvgw_prof GROUP BY profile'):
            collect_aws_vpn_infomation_by_profile(db, profile_row['profile'])

@add_logger
def collect_aws_vpn_infomation_by_profile(logger, db, profile):
    """
    AWS API を使って vgw_id、vgw_nm、vpc_id、vpn_nm の値から、VPN ID とそ
    の VPN のアドレスの一覧を取得します。
    """
    logger.debug(f'use {profile}')
    client = boto3.session.Session(profile_name=profile).client('ec2')
    with contextlib.closing(db.cursor()) as cur:
        # VPC の Name タグから VPC ID を得る。
        # https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2.html#EC2.Client.describe_vpcs
        filters = [{
            'Name': 'tag:Name',
            'Values': [row['vpc_nm'] for row in cur.execute(
                'SELECT vpc_nm FROM vpcvgw_vpc_nm'
                '   INNER JOIN host_vpcvgw_prof ON host_vpcvgw_prof.vpcvgw = vpcvgw_vpc_nm.vpcvgw'
                '   WHERE profile = ? GROUP BY vpc_nm;', (profile,))],
        }]
        for vpc in  client.describe_vpcs(Filters=filters).get('Vpcs', []):
            vpc_id = vpc.get('VpcId')
            if vpc_id:
                for tag in vpc.get('Tags', []):
                    if tag.get('Key') == 'Name':
                        vpc_nm = tag.get('Value')
                        break
                else:
                    vpc_nm = NONE
                if vpc_nm:
                        vpcvgw_list = [row['vpcvgw'] for row in cur.execute('SELECT vpcvgw FROM vpcvgw_vpc_nm WHERE vpc_nm = ?', (vpc_nm,))]
                        for vpcvgw in vpcvgw_list:
                            cur.execute(
                                'INSERT INTO vpcvgw_vpc_id (vpcvgw, vpc_id)'
                                '   VALUES (?, ?)', (vpcvgw, vpc_id))
                            logger.debug(f'add VPCVGW VPC_ID <{vpcvgw} {vpc_id}> to vpcvgw_vpc_id')
        # VPC ID から VGW ID を得る。
        # https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2.html#EC2.Client.describe_vpn_gateways
        filters = [{
            'Name': 'attachment.vpc-id',
            'Values': [row['vpc_id'] for row in cur.execute(
                'SELECT vpc_id FROM vpcvgw_vpc_id'
                '   INNER JOIN host_vpcvgw_prof ON host_vpcvgw_prof.vpcvgw = vpcvgw_vpc_id.vpcvgw'
                '   WHERE profile = ? GROUP BY vpc_id;', (profile,))],
        }]
        for vgw in client.describe_vpn_gateways(Filters=filters).get('VpnGateways', []):
            vgw_id = vgw.get('VpnGatewayId')
            if vgw_id:
                for vpc in vgw.get('VpcAttachments', []):
                    vpc_id = vpc.get('VpcId')
                    if vpc_id:
                            vpcvgw_list = [row['vpcvgw'] for row in cur.execute('SELECT vpcvgw FROM vpcvgw_vpc_id WHERE vpc_id = ?', (vpc_id,))]
                            for vpcvgw in vpcvgw_list:
                                cur.execute(
                                    'INSERT INTO vpcvgw_vgw_id (vpcvgw, vgw_id)'
                                    '   VALUES (?, ?)', (vpcvgw, vgw_id))
                                logger.debug(f'add VPCVGW VGW_ID <{vpcvgw} {vgw_id}> to vpcvgw_vgw_id')
        # VGW の Name タグから VGW ID を得る。
        # https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2.html#EC2.Client.describe_vpn_gateways
        filters = [{
            'Name': 'tag:Name',
            'Values': [row['vgw_nm'] for row in cur.execute(
                'SELECT vgw_nm FROM vpcvgw_vgw_nm'
                '   INNER JOIN host_vpcvgw_prof ON host_vpcvgw_prof.vpcvgw = vpcvgw_vgw_nm.vpcvgw'
                '   WHERE profile = ? GROUP BY vgw_nm;', (profile,))],
        }]
        for vgw in client.describe_vpn_gateways(Filters=filters).get('VpnGateways', []):
            vgw_id = vgw.get('VpnGatewayId')
            if vgw_id:
                for tag in vpc.get('Tags', []):
                    if tag.get('Key') == 'Name':
                        vgw_nm = tag.get('Value')
                        break
                else:
                    vgw_nm = None
                if vgw_nm:
                    vpcvgw_list = [row['vpcvgw'] for row in cur.execute('SELECT vpcvgw FROM vpcvgw_vgw_nm WHERE vgw_nm = ?', (vgw_nm,))]
                    for vpcvgw in vpcvgw_list:
                        cur.execute(
                            'INSERT INTO vpcvgw_vgw_id (vpcvgw, vgw_id)'
                            '   VALUES (?, ?)', (vpcvgw, vgw_id))
                        logger.debug(f'add VPCVGW VGW_ID <{vpcvgw} {vgw_id}> to vpcvgw_vgw_id')
        # VGW ID から VPN の情報を得る。
        # aws --profile PF1 --output json ec2 describe-vpn-connections --filters 'Name=vpn-gateway-id,Values=vgw-0322c028dd80839ae' | jq '[ .VpnConnections[] | { "VpnConnectionId": .VpnConnectionId, "VgwTelemetry": [ .VgwTelemetry[] | { "OutsideIpAddress": .OutsideIpAddress } ] } ]'
        filters = [{
            'Name': 'vpn-gateway-id',
            'Values': [row['vgw_id'] for row in cur.execute(
                'SELECT vgw_id FROM vpcvgw_vgw_id'
                '   INNER JOIN host_vpcvgw_prof ON host_vpcvgw_prof.vpcvgw = vpcvgw_vgw_id.vpcvgw'
                '   WHERE profile=? GROUP BY vgw_id;', (profile,))],
        }]
        for vpn in client.describe_vpn_connections(Filters=filters).get('VpnConnections', []):
            vgw_id = vpn.get('VpnGatewayId')
            vpn_id = vpn.get('VpnConnectionId')
            vpn_addr_list = [vgw_telemetry.get('OutsideIpAddress') for vgw_telemetry in vpn.get('VgwTelemetry',[]) if vgw_telemetry.get('OutsideIpAddress')]
            if vgw_id and vpn_id:
                vpcvgw_list = [row['vpcvgw'] for row in cur.execute('SELECT vpcvgw FROM vpcvgw_vgw_id WHERE vgw_id = ?', (vgw_id,))]
                vpn_nm = None
                for tag in vpn.get('Tags', []):
                    if tag.get('Key') == 'Name':
                        vpn_nm = tag.get('Value')
                        break
                else:
                    vpn_nm = 'NONAME'
                for vpcvgw in vpcvgw_list:
                    cur.execute(
                        'INSERT INTO vpcvgw_vpn (vpcvgw, vpn_id, vpn_nm)'
                        '   VALUES (?, ?, ?)', (vpcvgw, vpn_id, vpn_nm))
                    logger.debug(f'add VPCVGW VPN_ID VPN_NM <{vpcvgw} {vpn_id} {vpn_nm}> to vpcvgw_vpn')
                for addr in vpn_addr_list:
                    cur.execute(
                        'INSERT INTO vpn_addr (vpn_id, addr)'
                        '   VALUES (?, ?);', (vpn_id, addr)
                    )
                    logger.debug(f'add VPN_ID ADDR <{vpn_id} {addr}> to vpn_addr')

@add_logger
def build_sender_metrics(logger, db):
    packet = list()
    with contextlib.closing(db.cursor()) as cur:
        for row in cur.execute('SELECT host FROM host_vpcvgw_prof GROUP BY host;'):
            host_metric = build_host_metric(db, row['host'])
            if host_metric:
                packet.append(host_metric)
    for p in packet:
        logger.debug(f'{p}')
    return packet

@add_logger
def build_host_metric(logger, db, host):
    logger.debug(f'build sender metric for {host}')
    data = list()
    with contextlib.closing(db.cursor()) as cur:
        cur.execute(
            'SELECT vpn_id, vpn_nm FROM vpcvgw_vpn'
            '   INNER JOIN host_vpcvgw_prof ON host_vpcvgw_prof.vpcvgw = vpcvgw_vpn.vpcvgw'
            '   WHERE host = ?;', (host,)
        )
        for row in cur:
            vpn, name = row['vpn_id'], row['vpn_nm'],
            data.append({
                '{#VPN_ID}': vpn,
                '{#VPN_NAME}': name,
            })
            logger.debug(f'append metric data <{vpn} {name}>')
            data.extend(build_vpn_discovery_data(db, vpn, name))
    return ZabbixMetric(host, DISCOVERY_KEY, json.dumps(dict(data=data))) if data else None

@add_logger
def build_vpn_discovery_data(logger, db, vpn, name):
    data = list()
    with contextlib.closing(db.cursor()) as cur:
        for row in cur.execute('SELECT addr FROM vpn_addr WHERE vpn_id = ?;', (vpn,)):
            data.append({
                '{#VPN_CON_ID}': vpn,
                '{#VPN_CON_NAME}': name,
                '{#VPN_CON_ADDR}': row['addr'],
            })
            logger.debug(f'append metric data <{vpn} {name} {row["addr"]}>')
    return data

if __name__ == '__main__':
    loggingBasicConfig(format='%(levelname)s::%(funcName)s::%(message)s')
    main()