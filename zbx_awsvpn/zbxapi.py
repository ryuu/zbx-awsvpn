#!/usr/bin/env python3
#-*- coding: utf-8 -*-

import json
import requests
from requests import Session
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from urllib.parse import urljoin
import warnings

class ExecptZabbixApi(IOError):
    @classmethod
    def raiseExceptOnError(cls, json_response):
        if json_response.get('default') is None:
            return
        error = json_response['error']
        raise cls(
            error.get('code', -1),
            '{message}: {data}'.format(
                message=error.get('message', ''),
                data=error.get('data','')
            )
        )

class ZabbixApi:
    FILENAME_ZABBIX_API = 'api_jsonrpc.php'
    
    def __init__(self, host, user=None, password=None, verify=True, cert=None):
        """Zabbix API インスタンスを返す
        
        :param host: Zabbix サーバの URL (API リクエスト ファイル名は含まない)
        :param user: Zabbix API のアクセスユーザ
        :param password: Zabbix API のアクセスユーザパスワード
        :param verify: サーバー証明書の検証要否 (詳細は requests パッケージを参照)
            True - 検証する
            False - 検証しない
            パス - OpenSSL の c_rehash ユーティリティで処理された信頼できる CA 証明書リストのディレクトリ
        :param cert: クライアント証明書 (詳細は requests パッケージを参照)
        :return: -
        """
        self.request_id = 1
        self.uri = urljoin(host, self.FILENAME_ZABBIX_API)
        self.verify = verify
        self.session = Session()
        if cert is not None:
            self.session.cert = cert
        if user is not None and password is not None:
            self.login(user, password)
    
    def login(self, user, password):
        """Zabbix API にログインする
        
        :param user: Zabbix API のアクセスユーザ
        :param password: Zabbix API のアクセスユーザパスワード
        :return: アクセス トークン文字列
        """
        auth_token = self.do_request('user.login', {'user': user, 'password': password})
        if auth_token is not None:
            self.auth_token = auth_token
        return auth_token
    
    def do_request(self, method, params={}, auth_token=None):
        """Zabbix API にリクエストを送信する
        API の id は現行特に必要ないため単純にインクリメントした数値を代入している。
        
        :param method: Zabbix API のメソッド名
        :param params: Zabbix API のメソッドの引数
        :param auth_token: Zabbix API の認証トークン
        :return: 辞書形式の応答 (JSON-RPC2.0 形式の応答に含まれる result の値)
        """
        if auth_token is None and hasattr(self, 'auth_token'):
            auth_token = self.auth_token
        headers = {'Content-Type': 'application/json-rpc; charset=utf-8'}
        data = {'jsonrpc': '2.0',
                'method': method,
                'params': params,
                'auth': auth_token,
                'id': self.request_id}
        self.request_id += 1
        #以下コマンドでは、Ubuntu 14.04 で提供されている python3-request
        #2.2.1 の requests.Session().post() メソッドに json パラメーター
        #がないため、json.doums() メソッドを使用します。
        #requests.Session().post() メソッドで json パラメーターが使用でき
        #るのは 2.3.0 以降のようです。
        with warnings.catch_warnings():
            requests.packages.urllib3.disable_warnings(requests.packages.urllib3.exceptions.InsecureRequestWarning)
            response = self.session.post(self.uri, data=json.dumps(data, ensure_ascii=False).encode('utf8'), headers=headers, verify=self.verify)
        response.raise_for_status()
        try:
            json_response = response.json()
        except json.JSONDecodeError:
            raise
        ExecptZabbixApi.raiseExceptOnError(json_response)
        return json_response.get('result', [])
