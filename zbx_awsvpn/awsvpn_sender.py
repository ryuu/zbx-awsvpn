#!/opt/boto3/bin/python
#-*- coding: utf-8 -*-

import boto3
from botocore.exceptions import ClientError
import click
import contextlib
from datetime import datetime, timedelta
from dateutil import tz
from functools import wraps
from logging import basicConfig as loggingBasicConfig, getLogger, StreamHandler, WARNING, INFO, DEBUG
from pathlib import Path
import pprint
from pyzabbix.sender import ZabbixMetric, ZabbixSender
import re
import sqlite3
import textwrap
from urllib.error import URLError
from urllib.parse import urlsplit, urlunsplit
from .zbxapi import ZabbixApi

MACRO_PROFILE = '{$AWS_PROFILE}'
FIRST_DATA_ACUIRE_OFFSET = timedelta(minutes=15)
MARGIN_ACQUIRE_OCTETS = timedelta(minutes=1)
ITEM_KEY_VPN_STATUS = 'aws.vpnOperStatus'
ITEM_KEY_VPN_AGGR_STATUS = 'aws.vpnOperAggrStatus'
ITEM_KEY_VPN_IN_OCTETS = 'aws.vpnInOctets'
ITEM_KEY_VPN_IN_VOLUME = 'aws.vpnInVolume'
ITEM_KEY_VPN_OUT_OCTETS = 'aws.vpnOutOctets'
ITEM_KEY_VPN_OUT_VOLUME = 'aws.vpnOutVolume'
RE_ALL_ITEM_KEY = re.compile(r'(?P<key>aws\.vpn(?:OperStatus|(?P<dir>In|Out)(?:Octets|Volume)))\[(?P<id>[^,]+)(?:,(?P<addr>[^,]+))?]')

DATA_VOLUME_MODULO = 1000 * (1000 ** 3) # 1000G
TZ_LOCAL = tz.tzlocal()

def add_logger(func):
    BASENAME = None if __name__ is None else Path(__file__).stem if __name__== '__main__' else __name__
    logger = getLogger(BASENAME)
    if not logger.hasHandlers():
        logger.addHandler(StreamHandler())
    if func.__name__ != 'main':
        logger = logger.getChild(func.__name__)
    @wraps(func)
    def wrapper(*args, **kwds):
        return func(logger, *args, **kwds)
    return wrapper

@click.command()
@click.option('-r', '--uri', metavar='uri', default='http://localhost/zabbix/',
              help='Zabbix API URI.', show_default=True)
@click.option('-l', '--ssl', 'force_ssl', is_flag=True, default=False,
              help='Force SSL on Zabbix API URI.', show_default=True)
@click.option('-u', '--user', 'userid', metavar='USER ID', default='api',
              help='Login user name.', show_default=True)
@click.option('-p', '--passwd', metavar='PASSWORD', default='zabbix',
              help='Login user password', show_default=True)
@click.option('-t', '--send-to', 'send_to', default='localhost:10051',
              help='Zabbix Server address and port.', show_default=True)
@click.option('-v', '--verbose', count=True, default=0,
              help='Logging level, assigned -v or -vv.')
@add_logger
def main(logger, uri, force_ssl, userid, passwd, send_to, verbose):
    logger.setLevel({0: WARNING, 1: INFO, 2: DEBUG}.get(verbose, DEBUG))
    if force_ssl:
        uri_parts = urlsplit(uri, scheme='http')
        if uri_parts.scheme == 'http':
            uri = urlunsplit(['https'] + list(uri_parts[1:]))
    with create_database() as db:
        collect_item_infomation_from_zbx(db, uri, userid, passwd)
        collect_aws_vpn_infomation(db)
        packet = build_sender_metrics(db)
    if packet:
        sender_params = dict(
            zabbix_server='localhost',
            zabbix_port=10051,
        )
        send_to = send_to.rsplit(':', 1)
        if send_to:
            if send_to[0]:
                sender_params['zabbix_server'] = send_to[0]
            if len(send_to) > 1 and send_to[1].isdecimal():
                sender_params['zabbix_port'] = int(send_to[1])
        logger.info(f'send packet to "{sender_params["zabbix_server"]}:{sender_params["zabbix_port"]}"')
        result = ZabbixSender(**sender_params).send(packet)
        if result.failed:
            logger.error(f'{result}')
        else:
            logger.debug(f'{result}')

@add_logger
def create_database(logger):
    db = sqlite3.connect(':memory:', isolation_level=None)
    db.row_factory = sqlite3.Row
    sql_create_table = textwrap.dedent("""
        CREATE TABLE host_vpn_prof (
            hostid INTEGER,
            host TEXT,
            vpn TEXT,
            profile TEXT,
            PRIMARY KEY (hostid, vpn) ON CONFLICT IGNORE
        );
        CREATE INDEX idx_host ON host_vpn_prof (host);
        CREATE INDEX idx_vpn ON host_vpn_prof (vpn);
        CREATE INDEX idx_prof ON host_vpn_prof (profile);
        
        CREATE TABLE vpn_addr (
           vpn TEXT,
           addr TEXT,
           status INTEGER,
           PRIMARY KEY (vpn, addr) ON CONFLICT IGNORE
        );
        
        CREATE TABLE vpn_lastclock_data (
           vpn TEXT,
           direction TEXT,
           lastclock INTEGER,
           lastvalue INTEGER,
           PRIMARY KEY (vpn, direction) ON CONFLICT IGNORE
        );
        CREATE INDEX idx_lastclock ON vpn_lastclock_data (lastclock);
        
        CREATE TABLE vpn_octet (
           vpn TEXT,
           direction TEXT,
           acquireclock INTEGER,
           octets INTEGER,
           PRIMARY KEY (vpn, direction, acquireclock) ON CONFLICT IGNORE
        );
        """)
    with contextlib.closing(db.cursor()) as cur:
        cur.executescript(sql_create_table)
        if logger.getEffectiveLevel() >= DEBUG:
            cur.execute('select tbl_name from sqlite_master WHERE type="table";')
            for row in cur:
                logger.debug(f'sql table "{row["tbl_name"]}" created')
    return db

@add_logger
def collect_item_infomation_from_zbx(logger, db, uri, userid, passwd):
    """
    引数の uri, userid, passwd を使って Zabbix API にアクセスして、
    aws.vpnOperStatus、aws.vpnInOctets、aws.vpnOutOctets などがキーであ
    るトラッパー アイテムをすべて取得して、そのアイテムのパラメーターと、
    そのホストのマクロから AWS のプロファイル名を取得したうえで、db に登
    録します。
    """
    try:
        api = ZabbixApi(uri, user=userid, password=passwd, verify=False)
        logger.info(f'access to Zabbix API {uri} with "{userid}"')
    except URLError:
        logger.error(f'access to Zabbix API {uri} with "{userid}"')
        raise
    with contextlib.closing(db.cursor()) as cur:
        # 特定のキーで指定されるトラッパー アイテムとホストの一覧を取得す
        # る。
        method = 'item.get'
        params = {
            'monitored': True,
            'selectHosts': ['hostid', 'host'],
            'filter': {
                'type': 2,      #-- 2 is Zabbix trapper
                'status': 0,    #-- 0 is enabled item
            },
            'search': {
                'key_': [
                    ITEM_KEY_VPN_STATUS,
                    ITEM_KEY_VPN_AGGR_STATUS,
                    ITEM_KEY_VPN_IN_OCTETS,
                    ITEM_KEY_VPN_IN_VOLUME,
                    ITEM_KEY_VPN_OUT_OCTETS,
                    ITEM_KEY_VPN_OUT_VOLUME,
                ],
            },
            'searchByAny': True,
            'startSearch': True,
            'output': ['itemid', 'key_', 'lastclock', 'lastvalue', 'hosts'],
        }
        for item in api.do_request(method, params):
            match = RE_ALL_ITEM_KEY.match(item.get('key_', ''))
            if match:
                itemkey, direction, vpn_id, vpn_addr = match.group('key', 'dir', 'id', 'addr')
                if vpn_id:
                    for host_info in item.get('hosts'):
                        hostid = host_info.get('hostid')
                        host = host_info.get('host')
                        if hostid is not None and host:
                            cur.execute(
                                'INSERT INTO host_vpn_prof'
                                '   (hostid, host, vpn) VALUES'
                                '   (?, ?, ?);', (hostid, host, vpn_id)
                            )
                            logger.debug(f'add HOST VPN <{hostid} {host}, {vpn_id}> to host_vpn_prof')
                    if vpn_addr:
                        cur.execute(
                            'INSERT INTO vpn_addr'
                            '   (vpn, addr) VALUES'
                            '   (?, ?);', (vpn_id, vpn_addr)
                        )
                        logger.debug(f'add VPN ADDR <{vpn_id}, {vpn_addr}> to vpn_addr')
                    if itemkey in (ITEM_KEY_VPN_IN_VOLUME, ITEM_KEY_VPN_OUT_VOLUME) and direction:
                        direction = direction.lower()
                        lastclock = int(item.get('lastclock', '0'))
                        if lastclock:
                            lastclock = datetime.fromtimestamp(int(item.get('lastclock')))
                        else:
                            lastclock = datetime.now() - FIRST_DATA_ACUIRE_OFFSET
                        lastclock = lastclock.replace(second=0, microsecond=0, tzinfo=TZ_LOCAL)
                        try:
                            lastvalue = int(item.get('lastvalue'))
                        except (ValueError, TypeError):
                            lastvalue = 0
                        if direction in ('in', 'out'):
                            cur.execute(
                                'INSERT INTO vpn_lastclock_data'
                                '   (vpn, direction, lastclock, lastvalue) VALUES'
                                '   (?, ?, ? , ?)', (vpn_id, direction, lastclock.timestamp(), lastvalue)
                        )
                        logger.debug(f'add VPN DIR lastclock value <{vpn_id}, {direction}, {lastclock}, {lastvalue}> to vpn_lastclock_data')
        # 取得したホストで定義されているマクロ {$AWS_PROFILE} を取得す
        # る。
        cur.execute('select hostid from host_vpn_prof GROUP BY hostid;')
        method = 'usermacro.get'
        params = {
            'hostids': [row['hostid'] for row in cur.fetchall()],
            'filter': {
                'macro': MACRO_PROFILE,
            },
            'output': ['hostmacroid', 'hostid', 'macro', 'value'],
        }
        for usermacro in api.do_request(method, params):
            hostid , macro, value = usermacro.get('hostid'), usermacro.get('macro'), usermacro.get('value')
            if hostid and value:
                cur.execute('UPDATE host_vpn_prof SET profile = ? WHERE hostid = ?;', (value, hostid))
                logger.debug(f'update HOST VPN PROF <{hostid}, {vpn_id} {value}> to host_vpn_prof')

@add_logger
def collect_aws_vpn_infomation(logger, db):
    """
    AWS API を使って VPN の状態などを取得します。
    本関数の処理開始時刻を情報取得基準時刻にするため、下位関数に受け渡し
    ます。
    """
    executeclock = datetime.now().replace(second=0, microsecond=0, tzinfo=TZ_LOCAL) - MARGIN_ACQUIRE_OCTETS
    with contextlib.closing(db.cursor()) as cur:
        for profile_row in cur.execute('SELECT profile FROM host_vpn_prof GROUP BY profile'):
            collect_aws_vpn_infomation_by_profile(db, profile_row['profile'], executeclock)

@add_logger
def collect_aws_vpn_infomation_by_profile(logger, db, profile, executeclock):
    """
    AWS API を使って VPN の状態などを取得します。
    """
    logger.debug(f'use {profile}')
    collect_aws_vpn_status(db, profile)
    collect_aws_vpn_octet(db, profile, executeclock)

@add_logger
def collect_aws_vpn_status(logger, db, profile):
    """
    プロファイル毎に VPN の Up / Down の状態を取得します。
    """
    with contextlib.closing(db.cursor()) as cur:
        # VPN の状態を取得する。
        # https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2.html#EC2.Client.describe_vpn_connections
        logger.debug('getting vpn status')
        cur.execute(
            'SELECT vpn_addr.vpn as vpn FROM vpn_addr'
            '   INNER JOIN host_vpn_prof ON host_vpn_prof.vpn = vpn_addr.vpn'
            '   WHERE profile = ? GROUP BY vpn_addr.vpn;', (profile,))
        vpn_list = [row['vpn'] for row in cur]
        if vpn_list:
            client = boto3.session.Session(profile_name=profile).client('ec2')
            filters = [{
                'Name': 'vpn-connection-id',
                'Values': vpn_list,
            }]
            try:
                vpn_connections_description = client.describe_vpn_connections(Filters=filters)
            except ClientError as e:
                if e.response.get('Error', {}).get('Code', '') != 'InvalidVpnConnectionID.NotFound':
                    raise
                pass
            except:
                raise
            else:
                for vpn in vpn_connections_description.get('VpnConnections', []):
                    vpn_id = vpn.get('VpnConnectionId')
                    for telemetry in vpn.get('VgwTelemetry', []):
                        addr, status = telemetry.get('OutsideIpAddress'), telemetry.get('Status', 'DOWN')
                        status = 1 if status.upper() == 'UP' else 0
                        if vpn_id and addr:
                            cur.execute(
                                'UPDATE vpn_addr SET status = ? WHERE vpn = ? and addr = ?;', (status, vpn_id, addr)
                            )
                            logger.debug(f'add vpn status at {vpn_id} {addr} {"Up" if status else "Down"}')

@add_logger
def collect_aws_vpn_octet(logger, db, profile, executeclock):
    """
    プロファイル毎に VPN の 通信量を取得します。
    """
    with contextlib.closing(db.cursor()) as cur:
        # VPN の通信量を取得する。
        # https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/cloudwatch.html#CloudWatch.Client.get_metric_data
        logger.debug('getting vpn trafic')
        cur.execute(
            'SELECT lastclock from vpn_lastclock_data'
            '   INNER JOIN host_vpn_prof ON host_vpn_prof.vpn = vpn_lastclock_data.vpn'
            '   WHERE profile = ? GROUP BY lastclock ORDER BY lastclock DESC;', (profile,)
        )
        for row in cur:
            lastclock = datetime.fromtimestamp(int(row['lastclock']), tz=TZ_LOCAL)
            collect_aws_vpn_octet_same_lastclock(db, profile, lastclock, executeclock)

@add_logger
def collect_aws_vpn_octet_same_lastclock(logger, db, profile, lastclock, executeclock):
    span = (int((executeclock - lastclock).total_seconds()) // 60) * 60
    acquireclock = lastclock + timedelta(seconds=span)
    filters = list()
    with contextlib.closing(db.cursor()) as cur:
        cur.execute(
            'SELECT vpn_lastclock_data.vpn as vpn, direction FROM vpn_lastclock_data'
            '   INNER JOIN host_vpn_prof ON host_vpn_prof.vpn = vpn_lastclock_data.vpn'
            '   WHERE profile = ? and lastclock = ?;', (profile, lastclock.timestamp())
        )
        for row in cur:
            vpn_id, direction = row['vpn'], row['direction']
            filters.append({
                'Id': f'{vpn_id.replace("-", "_")}__{direction}',
                'MetricStat':{
                    'Metric':{
                        'Namespace': 'AWS/VPN',
                        'MetricName': f'TunnelData{direction.capitalize()}',
                        'Dimensions': [{
                            'Name': 'VpnId',
                            'Value': vpn_id,
                        }]
                    },
                    'Period': 60,
                    'Stat': 'Sum',
                    'Unit': 'Bytes',
                },
            })
    if filters:
        with contextlib.closing(db.cursor()) as cur:
            client = boto3.session.Session(profile_name=profile).client('cloudwatch')
            metric_data_results = client.get_metric_data(
                    MetricDataQueries=filters,
                    StartTime=lastclock.astimezone(tz.UTC).replace(tzinfo=None),
                    EndTime=acquireclock.astimezone(tz.UTC).replace(tzinfo=None)
                    ).get('MetricDataResults', [])
            for metric_data in metric_data_results:
                vpn_id, direction = (metric_data.get('Id', '').split('__') + [''])[:2]
                vpn_id = vpn_id.replace('_', '-')
                for clock, value in zip(metric_data.get('Timestamps', []), metric_data.get('Values', [])):
                    cur.execute(
                        'INSERT INTO vpn_octet'
                        '   (vpn, direction, acquireclock, octets) VALUES'
                        '   (?, ?, ?, ?);', (vpn_id, direction, clock.timestamp(), int(value))
                    )
                    logger.debug(f'add vpn octet at {vpn_id} {direction} {clock} {value}')

@add_logger
def build_sender_metrics(logger, db):
    packet = list()
    with contextlib.closing(db.cursor()) as cur:
        for row in cur.execute('SELECT host FROM host_vpn_prof GROUP BY host;'):
            host_metrics = build_host_metrics(db, row['host'])
            if host_metrics:
                packet.extend(host_metrics)
    for metric in packet:
        logger.debug(f'{metric}')
    return packet

@add_logger
def build_host_metrics(logger, db, host):
    metric_list = list()
    with contextlib.closing(db.cursor()) as cur:
        cur.execute(
            'SELECT vpn_addr.vpn as vpn, addr, status FROM vpn_addr'
            '   INNER JOIN host_vpn_prof ON host_vpn_prof.vpn = vpn_addr.vpn'
            '   WHERE host = ?;', (host,)
        )
        for row in cur:
            metric_list.append(ZabbixMetric(host, f'{ITEM_KEY_VPN_STATUS}[{row["vpn"]},{row["addr"]}]', row['status']))
        cur.execute(
            'SELECT vpn_addr.vpn as vpn, sum(status) as aggregate_status FROM vpn_addr'
            '   INNER JOIN host_vpn_prof ON host_vpn_prof.vpn = vpn_addr.vpn'
            '   WHERE host = ? GROUP BY vpn_addr.vpn;', (host,)
        )
        for row in cur:
            metric_list.append(ZabbixMetric(host, f'{ITEM_KEY_VPN_AGGR_STATUS}[{row["vpn"]}]', row['aggregate_status']))
        cur.execute(
            'SELECT vpn_octet.vpn as vpn FROM vpn_octet'
            '   INNER JOIN host_vpn_prof ON host_vpn_prof.vpn = vpn_octet.vpn'
            '   WHERE host = ? GROUP BY vpn_octet.vpn;', (host,)
        )
        for row in cur:
            vpn_octet_metric = build_vpn_octet_metrics(db, host, row['vpn'])
            if vpn_octet_metric:
                metric_list.extend(vpn_octet_metric)
    return metric_list

@add_logger
def build_vpn_octet_metrics(logger, db, host, vpn):
    metric_list = list()
    with contextlib.closing(db.cursor()) as cur:
        for direction, key in (('in', ITEM_KEY_VPN_IN_OCTETS), ('out', ITEM_KEY_VPN_OUT_OCTETS)):
            cur.execute(
                'SELECT acquireclock, octets FROM vpn_octet'
                '   WHERE vpn = ? and direction = ?'
                '   ORDER BY acquireclock DESC LIMIT 1;', (vpn, direction)
            )
            row = cur.fetchone()
            metric_list.append(ZabbixMetric(host, f'{key}[{vpn}]', round(int(row['octets']) / 60), datetime.fromtimestamp(row['acquireclock'], tz=TZ_LOCAL).timestamp()))
        for direction, key in (('in', ITEM_KEY_VPN_IN_VOLUME), ('out', ITEM_KEY_VPN_OUT_VOLUME)):
            cur.execute(
                'SELECT vpn_octet.vpn as vpn, max(acquireclock) as acquireclock, sum(octets) as volume, lastvalue'
                '   FROM vpn_octet INNER JOIN vpn_lastclock_data'
                '   ON vpn_lastclock_data.vpn = vpn_octet.vpn and vpn_lastclock_data.direction = vpn_octet.direction'
                '   WHERE vpn_octet.vpn = ? and vpn_octet.direction = ?;', (vpn, direction)
            )
            row = cur.fetchone()
            metric_list.append(ZabbixMetric(host, f'{key}[{vpn}]', (int(row['volume']) + int(row['lastvalue'])) % DATA_VOLUME_MODULO, datetime.fromtimestamp(row['acquireclock'], tz=TZ_LOCAL).timestamp()))
    return metric_list

if __name__ == '__main__':
    loggingBasicConfig(format='%(levelname)s::%(funcName)s::%(message)s')
    main()